import { hot } from "react-hot-loader/root";
import React, { Component } from "react";
import "./style.css";

class App extends Component {
    render() {
        return (
            <div className="App">
                <h1>Hello, World!!</h1>
                <input type="radio" id="check" name="check-hot" value="check" />
                <label for="check">Check hot</label>
            </div>
        )
    }
}

export default hot(App);
